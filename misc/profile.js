"use strict";

/**
 * Copyright 2019-2020, 2022 Camil Staps.
 *
 * This file is part of convertprofile.
 *
 * Convertprofile is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Convertprofile is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with convertprofile. If not, see <https://www.gnu.org/licenses/>.
 */

function toggle () {
	let toggler=this;
	let entry=toggler.parentNode.parentNode;

	var display='block';
	if (toggler.classList.contains ('open')) {
		display='none';
		toggler.innerHTML='&#x229e;';
	} else {
		toggler.innerHTML='&#x229f;';
	}
	toggler.classList.toggle ('open');
	toggler.classList.toggle ('closed');

	let children=entry.children;
	for (var i=1; i<children.length; i++)
		children[i].style.display=display;
}

let elems=document.getElementsByClassName ('toggler');
for (var i=0; i<elems.length; i++) {
	elems[i].onclick=toggle;
	elems[i].onclick();
}
