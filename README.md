# convertprofile

A small tool to convert [Clean][] callgraph profile files (`*.pgcl`) to
[Callgrind][] format (viewable with [KCachegrind][]) or HTML.

Use as `convertprofile module.pgcl` or
`convertprofile -o graph.html module.pgcl`; see `convertprofile --help` for
more options.

## Maintainer & license

This project is maintained by [Camil Staps][].

This project is licensed under AGPL v3; see the [LICENSE](/LICENSE) file.

[Callgrind]: https://valgrind.org/docs/manual/cl-manual.html
[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
[KCachegrind]: https://kcachegrind.github.io/html/Home.html
