implementation module HTML

/**
 * Copyright 2019-2020, 2022 Camil Staps.
 *
 * This file is part of convertprofile.
 *
 * Convertprofile is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Convertprofile is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with convertprofile. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Data.Error
import Data.Func
import System.File
import System.FilePath
import System.OS
import System.Process
import System._Pointer
from Text import class Text(concat), instance Text String
import Text.HTML

import PGCL

write_profile_to_html :: !Profile !*File !*World -> (!*File, !*World)
write_profile_to_html profile f w
	# (css,w) = readFile (exec_directory </> ".." </> "misc" </> "profile.css") w
	| isError css
		= abort ("Could not open supporting CSS: "+++toString (fromError css)+++"\n")
	# (js,w) = readFile (exec_directory </> ".." </> "misc" </> "profile.js") w
	| isError js
		= abort ("Could not open supporting JavaScript: "+++toString (fromError js)+++"\n")
	# f = write_profile (fromOk css) (fromOk js) profile f
	= (f,w)

write_profile :: !String !String !Profile !*File -> *File
write_profile css js p f
	# f = f <<< "<!DOCTYPE html>"
		<<< "<html lang=\"en\">"
		<<< "<head>"
		<<< "<title>Profile</title>"
		<<< "<meta charset=\"utf-8\">"
		<<< "<style type=\"text/css\">" <<< css <<< "</style>"
		<<< "</head>"
		<<< "<body>"
		<<< "<div class=\"header\">"
		<<< "<span class=\"entry-data\">Function</span>"
		<<< "<span class=\"progress-header\">Words</span>"
		<<< "<span class=\"progress-header\">Ticks</span>"
		<<< "<span class=\"entry-data\">Curried calls</span>"
		<<< "<span class=\"entry-data\">Lazy calls</span>"
		<<< "<span class=\"entry-data\">Strict calls</span>"
		<<< "</div>"
	# f = write_stack p p.profile f
	= f
		<<< "<script>" <<< js <<< "</script>"
		<<< "</body>"
		<<< "</html>"

write_stack :: !Profile !ProfileStack !*File -> *File
write_stack p s f = write s.cumulative_ticks s.cumulative_words s f
where
	write :: !Int !Int !ProfileStack !*File -> *File
	write parent_ticks parent_words s f
		# f = f <<< "<div class=\"entry\">"
			<<< "<div class=\"entry-header\">"
			<<< if (isEmpty s.children) "" "<span class=\"toggler open\">&#x229f;</span>"
			<<< "<span class=\"entry-content\">" <<< escapeForTextNode p.modules.[cost_centre.cc_module]
				<<< ": <span class=\"function\">" <<< escapeForTextNode cost_centre.cc_name <<< "</span></span>"
			<<< "&nbsp;"
			<<< "<span class=\"progress words\" title=\"" <<< words <<< "\">"
				<<< if (parent_words==0) "" (concat ["<span class=\"bar\" style=\"",words_width,"\"></span>"])
				<<< "</span>"
			<<< "<span class=\"progress ticks\" title=\"" <<< ticks <<< "\">"
				<<< if (parent_ticks==0) "" (concat ["<span class=\"bar\" style=\"",ticks_width,"\"></span>"])
				<<< "</span>"
			<<< "<span class=\"entry-data\">" <<< if (s.ccalls==0) "&nbsp;" (toString s.ccalls) <<< "</span>"
			<<< "<span class=\"entry-data\">" <<< if (s.lcalls==0) "&nbsp;" (toString s.lcalls) <<< "</span>"
			<<< "<span class=\"entry-data\">" <<< if (s.scalls==0) "&nbsp;" (toString s.scalls) <<< "</span>"
			<<< "</div>"
		# f = foldl (\f c -> write s.cumulative_ticks s.cumulative_words c f) f s.children
		= f <<< "</div>"
	where
		cost_centre = p.cost_centres.[s.cost_centre]

		ticks = if (isEmpty s.children)
			(toString s.ticks)
			(concat [toString s.cumulative_ticks," (",toString s.ticks,")"])
		ticks_width = toWidth (toReal s.cumulative_ticks / toReal parent_ticks)

		words = if (isEmpty s.children)
			(toString s.words)
			(concat [toString s.cumulative_words," (",toString s.words,")"])
		words_width = toWidth (toReal s.cumulative_words / toReal parent_words)

		toWidth :: !Real -> String
		toWidth w
			| wi>=10000
				= "width:100%;"
				=
					{ 'w','i','d','t','h',':'
					, toChar (wi/1000)+'0'
					, toChar ((wi rem 1000)/100)+'0'
					, '.'
					, toChar ((wi rem 100)/10)+'0'
					, toChar (wi rem 10)+'0'
					, '%',';'
					}
		where
			wi = toInt (w * 10000.0)

exec_directory :: String
exec_directory =: takeDirectory (IF_LINUX get_linux (IF_WINDOWS get_windows get_macosx))
where
	get_linux
		# buf = createArray 8192 '\0'
		# n = readlink "/proc/self/exe\0" (get_string_pointer buf) 8192
		| n<0 || n>=8192
			= abort "exec_directory failed\n"
			= buf % (0,n-1)
	where
		readlink :: !String !Pointer !Int -> Int
		readlink _ _ _ = code {
			ccall readlink "spI:I"
		}

	get_windows
		# buf = createArray 8192 '\0'
		# n = GetModuleFileNameA 0 (get_string_pointer buf) 8192
		| n==0
			= abort "exec_directory failed"
			= buf % (0,n-1)
	where
		GetModuleFileNameA :: !Int !Pointer !Int -> Int
		GetModuleFileNameA _ _ _ = code {
			ccall GetModuleFileNameA "IpI:I"
		}

	get_macosx
		# buf = createArray 8192 '\0'
		# bufp = get_string_pointer buf
		# sz = createArray 1 8192
		# n = get_executable_path bufp (get_array_pointer sz)
		| n<>0
			= abort "exec_directory failed\n"
		# buf2 = createArray 8192 '\0'
		# buf2p = get_string_pointer buf2
		# r = realpath bufp buf2p
		| r<>r || buf.[0]=='\0' || buf2.[0]=='\0' /* keep buf and buf2 in memory */
			= abort "exec_directory failed\n"
			= derefString buf2p
	where
		get_executable_path :: !Pointer !Pointer -> Int
		get_executable_path _ _ = code {
			ccall _NSGetExecutablePath "pp:I"
		}

		realpath :: !Pointer !Pointer -> Pointer
		realpath _ _ = code {
			ccall realpath "pp:p"
		}

	get_string_pointer :: !String -> Pointer
	get_string_pointer arr = get arr + IF_INT_64_OR_32 16 8
	where
		get :: !String -> Pointer
		get _ = code {
			push_a_b 0
			pop_a 1
		}

	get_array_pointer :: !{#Int} -> Pointer
	get_array_pointer arr = get arr + IF_INT_64_OR_32 24 12
	where
		get :: !{#Int} -> Pointer
		get _ = code {
			push_a_b 0
			pop_a 1
		}

open_file_in_browser :: !String !*World -> *World
open_file_in_browser file w = snd (callProcess open args ?None w)
where
	open = IF_LINUX "xdg-open" (IF_WINDOWS "C:\\Windows\\System32\\cmd.exe" "open")
	args = IF_WINDOWS ["/c","start",file] [file]
