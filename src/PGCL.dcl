definition module PGCL

/**
 * Copyright 2019-2020, 2022 Camil Staps.
 *
 * This file is part of convertprofile.
 *
 * Convertprofile is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Convertprofile is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with convertprofile. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.Error import :: MaybeError
from Data.Set import :: Set

:: Profile =
	{ modules                 :: !.{#String}
	, cost_centres            :: !.{#CostCentre}
	, profile                 :: !ProfileStack
	, overhead_per_1000_calls :: !Int
	, cpu_frequency           :: !Int
	}

:: CostCentre =
	{ cc_module   :: !Int
	, cc_name     :: !String
	}

:: ProfileStack =
	{ cost_centre      :: !Int
	, ticks            :: !Int
	, words            :: !Int
	, scalls           :: !Int
	, lcalls           :: !Int
	, ccalls           :: !Int
	, children         :: ![ProfileStack]
	// Aggregates:
	, cumulative_ticks :: !Int
	, cumulative_words :: !Int
	, profiler_calls   :: !Int
	}

read_profile :: !*File -> (!MaybeError String Profile, !*File)

/**
 * Perform a number of analyses on a raw profile:
 *  - Remove excluded functions.
 *  - Lift all garbage collector entries to the top level.
 *  - Compute aggregates `cumulative_ticks` and `cumulative_words`.
 *  - Prune small subtrees (based on minimum cumulative ticks).
 *  - Correct for estimated profiling overhead.
 *
 * @param Minimum number of (cumulative) ticks. Cost centres with fewer ticks
 *   will be pruned from the tree.
 * @param Minimum number of (cumulative) words. Cost centres with fewer words
 *   will be pruned from the tree.
 * @param Functions to exclude, in the format MODULE:FUNCTION.
 */
prepare :: !Int !Int !(Set String) !Profile -> Profile
