definition module Callgrind

/**
 * Copyright 2019-2020, 2022 Camil Staps.
 *
 * This file is part of convertprofile.
 *
 * Convertprofile is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Convertprofile is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with convertprofile. If not, see <https://www.gnu.org/licenses/>.
 */

from PGCL import :: Profile

write_profile_to_callgrind :: !Profile !*File -> *File
