module convertprofile

/**
 * Copyright 2019-2020, 2022 Camil Staps.
 *
 * This file is part of convertprofile.
 *
 * Convertprofile is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Convertprofile is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with convertprofile. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Data.Error
import Data.Func
import qualified Data.Set
import System.CommandLine
import System.FilePath
import System.Options
from Text import class Text(concat,join), instance Text String

import Callgrind
import HTML
import PGCL

:: Options =
	{ input                :: !FilePath
	, output               :: !FilePath
	, min_cumulative_ticks :: !Int
	, min_cumulative_words :: !Int
	, exclude              :: !'Data.Set'.Set String
	}
defaultOptions =
	{ input                = ""
	, output               = "callgrind.out"
	, min_cumulative_ticks = 0
	, min_cumulative_words = 0
	, exclude              = 'Data.Set'.newSet
	}

Start w
	# ([prog:args],w) = getCommandLine w
	# args = parseOptions option_description args defaultOptions
	| isError args
		= exit (join "\n" [usage prog,"":fromError args]) w
	# args = fromOk args
	| size args.input==0
		= exit "Specify an input file" w
	# args & output = if (size args.output==0)
		(addExtension args.input "html")
		args.output
	# (ok,input,w) = fopen args.input FReadData w
	| not ok
		= exit "Could not open input file" w
	# (profile,input) = read_profile input
	# (_,w) = fclose input w
	| isError profile
		= exit ("Could not parse input: "+++fromError profile) w
	# profile = prepare args.min_cumulative_ticks args.min_cumulative_words args.exclude (fromOk profile)
	# (ok,f,w) = fopen args.output FWriteText w
	| not ok
		= exit "Could not open output file" w
	| args.output % (size args.output-5,size args.output-1) == ".html"
		# (f,w) = write_profile_to_html profile f w
		# (_,w) = fclose f w
		# w = open_file_in_browser args.output w
		= w
	| otherwise
		# f = write_profile_to_callgrind profile f
		# (_,w) = fclose f w
		= w
where
	usage prog = concat
		[ "Usage: "
		, prog
		, " [options] INPUT"
		]
	option_description =
		WithHelp True $ Options
		[ Shorthand "-o" "--output" $ Option "--output"
			(\op opts -> Ok {opts & output=op})
			"OUTPUT"
			"The output file (either *.html or callgrind.out[.*])"
		, Shorthand "-e" "--exclude" $ Option "--exclude"
			(\e opts -> Ok {opts & exclude='Data.Set'.insert e opts.exclude})
			"MODULE:FUNCTION"
			"A function to exclude (may be specified multiple times)"
		, Option "--min-ticks"
			(\t opts -> case toInt t of
				0
					| t=="0"
						-> Ok {opts & min_cumulative_ticks=0}
						-> Error [concat ["--min-ticks: invalid integer '",t,"'"]]
				t
					-> Ok {opts & min_cumulative_ticks=t})
			"N"
			"Only include cost centres with at least N ticks (cumulatively)"
		, Option "--min-words"
			(\t opts -> case toInt t of
				0
					| t=="0"
						-> Ok {opts & min_cumulative_words=0}
						-> Error [concat ["--min-words: invalid integer '",t,"'"]]
				t
					-> Ok {opts & min_cumulative_words=t})
			"N"
			"Only include cost centres with at least N words (cumulatively)"
		, Operand
			False
			(\ip opts
				| size opts.input==0
					-> ?Just (Ok {opts & input=ip})
					-> ?None)
			"INPUT"
			"The input file"
		]

	exit err w
		# (_,w) = fclose (stderr <<< err <<< '\n') w
		= setReturnCode -1 w
